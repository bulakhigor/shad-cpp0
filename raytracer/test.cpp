#include <catch.hpp>

#include <cmath>
#include <string>
#include <optional>

#include <raytracer.h>
#include <image.h>
#include <camera_options.h>
#include <render_options.h>

double PixelDistance(const RGB& lhs, const RGB& rhs) {
    return sqrt(std::pow(lhs.r - rhs.r, 2.0) + std::pow(lhs.g - rhs.g, 2.0) +
                std::pow(lhs.b - rhs.b, 2.0));
}

void Compare(const Image& actual, const Image& expected) {
    static const double kEps = 1e-3;
    int matches = 0;

    REQUIRE(actual.Width() == expected.Width());
    REQUIRE(actual.Height() == expected.Height());
    for (int y = 0; y < actual.Height(); ++y) {
        for (int x = 0; x < actual.Width(); ++x) {
            auto actual_data = actual.GetPixel(y, x);
            auto expected_data = expected.GetPixel(y, x);
            auto diff = PixelDistance(actual_data, expected_data);
            matches += diff < kEps;
        }
    }

    double similarity = static_cast<double>(matches) / (actual.Width() * actual.Height());
    REQUIRE(similarity >= 0.95);
}

void CheckImage(const std::string& obj_filename, const std::string& result_filename,
                const CameraOptions& camera_options, const RenderOptions& render_options,
                std::optional<std::string> output_filename = std::nullopt) {
    auto image = Render("tests/" + obj_filename, camera_options, render_options);
    Image ok_image("tests/" + result_filename);
    Compare(image, ok_image);
    if (output_filename.has_value()) {
        image.Write(output_filename.value());
    }
}

TEST_CASE("Box with spheres", "[raytracer]") {
    CameraOptions camera_opts(640, 480);
    camera_opts.look_from = std::array<double, 3>{0.0, 1.0, 0.98};
    camera_opts.look_to = std::array<double, 3>{0.0, 1.0, 0.0};
    RenderOptions render_opts{3};
    CheckImage("box/cube.obj", "box/cube.png", camera_opts, render_opts);
}

TEST_CASE("Shading parts", "[raytracer][asan]") {
    CameraOptions camera_opts(640, 480);
    RenderOptions render_opts{1};
    CheckImage("shading_parts/scene.obj", "shading_parts/scene.png", camera_opts, render_opts);
}